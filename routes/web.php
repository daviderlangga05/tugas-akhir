<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('frontpage.layout.home');
});*/

Route::get('/', 'FrontEnd\HomeController@index');
Route::get('/artikel/detail/{idPertanyaan}/{kategori}/{linkSeo}', 'FrontEnd\HomeController@artikel');
Route::get('/artikel/kategori/{idKategori}/{kategori}', 'FrontEnd\HomeController@kategori');
Route::get('/galeri', 'FrontEnd\HomeController@galeri');
Route::post('/komentar', 'FrontEnd\HomeController@store');


Route::get('/reset', function () {
    return view('auth.passwords.email');
});

Route::get('/recover', function () {
    return view('auth.passwords.reset');
});






Route::get('/content', function () {
    return view('frontpage.layout.content');
});


// auth route 
Route::get('/login', 'AuthController@login')->name('login');
Route::post('/postlogin', 'AuthController@postlogin');
Route::get('/logout', 'AuthController@logout');
Route::post('/signup', 'AuthController@signup');

// catatan route yg masuk admin panel dimasukkan kedalam route group ini!!
Route::group(['middleware' => 'auth'], function () {
    Route::get('/admin', 'AdminController@index')->name('HOME');
});


Route::resource('/admin/artikel', 'ArtikelController');
Route::resource('/admin/kategori', 'kategoriController');
Route::resource('/admin/komentarArtikel', 'komentarArtikelController');
Route::resource('/admin/foto', 'fotoController');
Route::resource('/admin/iklan', 'iklanController');
Route::resource('/admin/user', 'userController');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
