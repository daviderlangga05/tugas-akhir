@extends('frontpage.master')
@push('Ekko-Lightbox')
    <link href ="https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.css" rel = "stylesheet" crossorigin="anonymous">
    <style>
    .imggallery{
    max-height: 250px;
}
</style>
@endpush

<!-- Header -->


<header id="header" class="ex-header">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>Galeri</h1>
      </div> <!-- end of col -->
    </div> <!-- end of row -->
  </div> <!-- end of container -->
</header> <!-- end of ex-header -->
<!-- end of header -->


<!-- Breadcrumbs -->
<div class="ex-basic-1">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="breadcrumbs">
          <a href="/">Home</a><i class="fa fa-angle-double-right"></i><span>Galeri</span>
        </div> <!-- end of breadcrumbs -->
      </div> <!-- end of col -->
    </div> <!-- end of row -->
  </div> <!-- end of container -->
</div> <!-- end of ex-basic-1 -->
<!-- end of breadcrumbs -->


<!-- Privacy Content -->
<div class="ex-basic-2">
  <div class="container">
    <div class="row">
      <div class="col-lg-10 offset-lg-1">
        
      <div id = "gallery">
        <div class = "row text-center">
        @foreach($queryfoto as $key => $foto )
			<div class = "col-md-4">
				<a href = "{{'/galeri_foto/'.$foto -> foto}}" data-toggle = "lightbox" data-gallery="gallery">
					<img src = "{{'/galeri_foto/'.$foto -> foto}}" class= "imggallery">
				</a>
         <p> {{$foto -> judul_foto}} </p>
			</div>
      @endforeach

		</div>
    </div>
       
      </div> <!-- end of col-->
    </div> <!-- end of row -->
  </div> <!-- end of container -->
</div> <!-- end of ex-basic-2 -->
<!-- end of privacy content -->


<!-- Footer -->
<div class="footer">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <div class="footer-col">
          <h4>About Evolo</h4>
          <p>We're passionate about offering the best human resource services to our customers</p>
        </div>
      </div> <!-- end of col -->
      <div class="col-md-4">
        <div class="footer-col middle">
          <h4>Important Links</h4>
          <ul class="list-unstyled li-space-lg">
            <li class="media">
              <i class="fas fa-square"></i>
              <div class="media-body">Our business partners <a class="turquoise" href="#your-link">startupguide.com</a></div>
            </li>
            <li class="media">
              <i class="fas fa-square"></i>
              <div class="media-body">Read our <a class="turquoise" href="terms-conditions.html">Terms & Conditions</a>, <a class="turquoise" href="privacy-policy.html">Privacy Policy</a></div>
            </li>
          </ul>
        </div>
      </div> <!-- end of col -->
      <div class="col-md-4">
        <div class="footer-col last">
          <h4>Social Media</h4>
          <span class="fa-stack fa-lg">
            <a href="#your-link">
              <i class="fas fa-circle fa-stack-2x"></i>
              <i class="fab fa-facebook-f fa-stack-1x"></i>
            </a>
          </span>
          <span class="fa-stack fa-lg">
            <a href="#your-link">
              <i class="fas fa-circle fa-stack-2x"></i>
              <i class="fab fa-twitter fa-stack-1x"></i>
            </a>
          </span>
          <span class="fa-stack fa-lg">
            <a href="#your-link">
              <i class="fas fa-circle fa-stack-2x"></i>
              <i class="fab fa-google-plus-g fa-stack-1x"></i>
            </a>
          </span>
          <span class="fa-stack fa-lg">
            <a href="#your-link">
              <i class="fas fa-circle fa-stack-2x"></i>
              <i class="fab fa-instagram fa-stack-1x"></i>
            </a>
          </span>
          <span class="fa-stack fa-lg">
            <a href="#your-link">
              <i class="fas fa-circle fa-stack-2x"></i>
              <i class="fab fa-linkedin-in fa-stack-1x"></i>
            </a>
          </span>
        </div>
      </div> <!-- end of col -->
    </div> <!-- end of row -->
  </div> <!-- end of container -->
</div> <!-- end of footer -->
<!-- end of footer -->

@push('script-Ekko-Lightbox')
<script src = "https://cdnjs.cloudflare.com/ajax/libs/ekko-lightbox/5.3.0/ekko-lightbox.js" crossorigin="anonymous"></script>

<script>
  $(document).on("click", '[data-toggle="lightbox"]', function(event){
    event.preventDefault();
    $(this).ekkoLightbox();
  });
</script>
@endpush()


