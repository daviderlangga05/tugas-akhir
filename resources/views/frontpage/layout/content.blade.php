@extends('frontpage.master')
<!-- Header -->


<header id="header" class="ex-header">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>Privacy Policy</h1>
      </div> <!-- end of col -->
    </div> <!-- end of row -->
  </div> <!-- end of container -->
</header> <!-- end of ex-header -->
<!-- end of header -->


<!-- Breadcrumbs -->
<div class="ex-basic-1">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="breadcrumbs">
          <a href="/">Home</a><i class="fa fa-angle-double-right"></i><span>Privacy Policy</span>
        </div> <!-- end of breadcrumbs -->
      </div> <!-- end of col -->
    </div> <!-- end of row -->
  </div> <!-- end of container -->
</div> <!-- end of ex-basic-1 -->
<!-- end of breadcrumbs -->


<!-- Privacy Content -->
<div class="ex-basic-2">
  <div class="container">
    <div class="row">
      <div class="col-lg-10 offset-lg-1">
        <div class="text-container">
          <h3>Private Data We Receive And Collect</h3>
          <p>Evolo also automatically collects and receives certain information from your computer or mobile device, including the activities you perform on our Website, the Platforms, and the Applications, the type of hardware and software you are using (for example, your operating system or browser), and information obtained from cookies. For example, each time you visit the Website or otherwise use the Services, we automatically collect your IP address, browser and device type, access times, the web page from which you came, the regions from which you navigate the web page, and the web page(s) you access (as applicable).</p>
          <p>When you first register for a Evolo account, and when you use the Services, we collect some <a class="turquoise" href="#your-link">Personal Information</a> about you such as:</p>
          <div class="row">
            <div class="col-md-6">
              <ul class="list-unstyled li-space-lg indent">
                <li class="media">
                  <i class="fas fa-square"></i>
                  <div class="media-body">The geographic area where you use your computer and mobile devices</div>
                </li>
                <li class="media">
                  <i class="fas fa-square"></i>
                  <div class="media-body">Your full name, username, and email address and other contact details</div>
                </li>
                <li class="media">
                  <i class="fas fa-square"></i>
                  <div class="media-body">A unique Evolo user ID (an alphanumeric string) which is assigned to you upon registration</div>
                </li>
                <li class="media">
                  <i class="fas fa-square"></i>
                  <div class="media-body">Other optional information as part of your account profile</div>
                </li>
                <li class="media">
                  <i class="fas fa-square"></i>
                  <div class="media-body">Your IP Address and, when applicable, timestamp related to your consent and confirmation of consent</div>
                </li>
                <li class="media">
                  <i class="fas fa-square"></i>
                  <div class="media-body">Other information submitted by you or your organizational representatives via various methods</div>
                </li>
              </ul>
            </div> <!-- end of col -->

            <div class="col-md-6">
              <ul class="list-unstyled li-space-lg indent">
                <li class="media">
                  <i class="fas fa-square"></i>
                  <div class="media-body">Your billing address and any necessary other information to complete any financial transaction, and when making purchases through the Services, we may also collect your credit card or PayPal information</div>
                </li>
                <li class="media">
                  <i class="fas fa-square"></i>
                  <div class="media-body">User generated content (such as messages, posts, comments, pages, profiles, images, feeds or communications exchanged on the Supported Platforms)</div>
                </li>
                <li class="media">
                  <i class="fas fa-square"></i>
                  <div class="media-body">Images or other files that you may publish via our Services</div>
                </li>
                <li class="media">
                  <i class="fas fa-square"></i>
                  <div class="media-body">Information (such as messages, posts, comments, pages, profiles, images) we may receive relating to communications you send us, such as queries or comments concerning</div>
                </li>
              </ul>
            </div> <!-- end of col -->
          </div> <!-- end of row -->
        </div> <!-- end of text-container-->

       
      </div> <!-- end of col-->
    </div> <!-- end of row -->
  </div> <!-- end of container -->
</div> <!-- end of ex-basic-2 -->
<!-- end of privacy content -->


<!-- Footer -->
<div class="footer">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <div class="footer-col">
          <h4>About Evolo</h4>
          <p>We're passionate about offering the best human resource services to our customers</p>
        </div>
      </div> <!-- end of col -->
      <div class="col-md-4">
        <div class="footer-col middle">
          <h4>Important Links</h4>
          <ul class="list-unstyled li-space-lg">
            <li class="media">
              <i class="fas fa-square"></i>
              <div class="media-body">Our business partners <a class="turquoise" href="#your-link">startupguide.com</a></div>
            </li>
            <li class="media">
              <i class="fas fa-square"></i>
              <div class="media-body">Read our <a class="turquoise" href="terms-conditions.html">Terms & Conditions</a>, <a class="turquoise" href="privacy-policy.html">Privacy Policy</a></div>
            </li>
          </ul>
        </div>
      </div> <!-- end of col -->
      <div class="col-md-4">
        <div class="footer-col last">
          <h4>Social Media</h4>
          <span class="fa-stack fa-lg">
            <a href="#your-link">
              <i class="fas fa-circle fa-stack-2x"></i>
              <i class="fab fa-facebook-f fa-stack-1x"></i>
            </a>
          </span>
          <span class="fa-stack fa-lg">
            <a href="#your-link">
              <i class="fas fa-circle fa-stack-2x"></i>
              <i class="fab fa-twitter fa-stack-1x"></i>
            </a>
          </span>
          <span class="fa-stack fa-lg">
            <a href="#your-link">
              <i class="fas fa-circle fa-stack-2x"></i>
              <i class="fab fa-google-plus-g fa-stack-1x"></i>
            </a>
          </span>
          <span class="fa-stack fa-lg">
            <a href="#your-link">
              <i class="fas fa-circle fa-stack-2x"></i>
              <i class="fab fa-instagram fa-stack-1x"></i>
            </a>
          </span>
          <span class="fa-stack fa-lg">
            <a href="#your-link">
              <i class="fas fa-circle fa-stack-2x"></i>
              <i class="fab fa-linkedin-in fa-stack-1x"></i>
            </a>
          </span>
        </div>
      </div> <!-- end of col -->
    </div> <!-- end of row -->
  </div> <!-- end of container -->
</div> <!-- end of footer -->
<!-- end of footer -->