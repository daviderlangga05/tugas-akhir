@extends('frontpage.master')
<!-- Header -->
<header id="header" class="header">
  <div class="header-content">
    <div class="container">
      <div class="row">
        <div class="col-lg-6">
          <div class="text-container">
            <h1><span class="turquoise">Selamat Datang</span> di Blogger Milenial</h1>
            <p class="p-large">Pengetahuan yang sebenarnya ada saat kita menyadari tidak mengetahui apapun</p>
            <a class="btn-solid-lg page-scroll" href="#kategori">KLIK ARTIKEL PILIHAN KAMI</a>
          </div> <!-- end of text-container -->
        </div> <!-- end of col -->
        <div class="col-lg-6">
          <div class="image-container">
            <img class="img-fluid" src="{{asset('frontpage_asset/images/header-teamwork.svg')}}" alt="alternative">
          </div> <!-- end of image-container -->
        </div> <!-- end of col -->
      </div> <!-- end of row -->
    </div> <!-- end of container -->
  </div> <!-- end of header-content -->
</header> <!-- end of header -->
<!-- end of header -->


<!-- Services -->
<div id="kategori" class="cards-1">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h2>YUK, MEMBACA!</h2>
        <p class="p-heading p-large">Tuhan Maha Tahu, Tapi Manusia Harus dikasih Tahu..Manfaat Membaca Tidak Hanya Menambah Pengentahuan Tapi Lebih Dari Itu</p>
      </div> <!-- end of col -->
    </div> <!-- end of row -->
    <div class="row">
      <div class="col-lg-12">
        <!-- Card -->
        @foreach($queryKategori as $key => $kategori )
        <div class="card">
          <img class="card-image" src="{{asset('frontpage_asset/images/services-icon-3.svg')}}" alt="alternative">
          <div class="card-body">
            <h4 class="card-title">{{$kategori -> nama_kategori}}</h4>
           <p>With all the information in place you will be presented with an action plan that your company needs to follow</p>
          </div>
        </div>
        <!-- end of card -->
        @endforeach
      </div> <!-- end of col -->
    </div> <!-- end of row -->
    

  </div> <!-- end of container -->
</div> <!-- end of cards-1 -->
<!-- end of services -->
@foreach($queryArtikel as $key => $artikel )
<div class="container" style="border: 1px solid #c4d8dc; border-radius: 0.5rem; margin-bottom:30px">
<div class="text-container" style="padding:12px">
          <h3>{{$artikel -> judul}}</h3>
          <img src ="gambar_artikel/{{$artikel -> gambar}}" width="200px" 
          style="float:left; padding-right:20px;
            padding-bottom:px;
            padding-left:5px;">
          <p >{!!Str::limit ($artikel -> isi,250, ' ....')!!}</p> 
          <a href="/artikel/detail/{{$artikel -> id}}/{{$artikel->kategori->link_seo}}/{{$artikel -> link_seo}}"> <i>lihat Selengkapnya</i> </a>
          <hr>
          <i>Author : {{$artikel ->author->name}} || {{$artikel -> created_at}}</i>
        </div> <!-- end of text-container -->  
</div>
@endforeach

<!-- Testimonials -->
<div class="slider-2">
  <div class="container">
    <div class="row">
      <div class="col-lg-6">
        <div class="image-container">
          <img class="img-fluid" src="{{asset('frontpage_asset/images/testimonials-2-men-talking.svg')}}" alt="alternative">
        </div> <!-- end of image-container -->
      </div> <!-- end of col -->
      <div class="col-lg-6">
        <h2>Testimonials</h2>

        <!-- Card Slider -->
        <div class="slider-container">
          <div class="swiper-container card-slider">
            <div class="swiper-wrapper">

              <!-- Slide -->
              <div class="swiper-slide">
                <div class="card">
                  <img class="card-image" src="{{asset('frontpage_asset/images/testimonial-1.svg')}}" alt="alternative">
                  <div class="card-body">
                    <p class="testimonial-text">I just finished my trial period and was so amazed with the support and results that I purchased Evolo right away at the special price.</p>
                    <p class="testimonial-author">Jude Thorn - Designer</p>
                  </div>
                </div>
              </div> <!-- end of swiper-slide -->
              <!-- end of slide -->

              <!-- Slide -->
              <div class="swiper-slide">
                <div class="card">
                  <img class="card-image" src="{{asset('frontpage_asset/images/testimonial-2.svg')}}" alt="alternative">
                  <div class="card-body">
                    <p class="testimonial-text">Evolo has always helped or startup to position itself in the highly competitive market of mobile applications. You will not regret using it!</p>
                    <p class="testimonial-author">Marsha Singer - Developer</p>
                  </div>
                </div>
              </div> <!-- end of swiper-slide -->
              <!-- end of slide -->

              <!-- Slide -->
              <div class="swiper-slide">
                <div class="card">
                  <img class="card-image" src="{{asset('frontpage_asset/images/testimonial-3.svg')}}" alt="alternative">
                  <div class="card-body">
                    <p class="testimonial-text">Love their services and was so amazed with the support and results that I purchased Evolo for two years in a row. They are awesome.</p>
                    <p class="testimonial-author">Roy Smith - Marketer</p>
                  </div>
                </div>
              </div> <!-- end of swiper-slide -->
              <!-- end of slide -->

            </div> <!-- end of swiper-wrapper -->

            <!-- Add Arrows -->
            <div class="swiper-button-next"></div>
            <div class="swiper-button-prev"></div>
            <!-- end of add arrows -->

          </div> <!-- end of swiper-container -->
        </div> <!-- end of slider-container -->
        <!-- end of card slider -->

      </div> <!-- end of col -->
    </div> <!-- end of row -->
  </div> <!-- end of container -->
</div> <!-- end of slider-2 -->
<!-- end of testimonials -->


<!-- About -->
<div id="about" class="basic-4">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <h2>About The Team</h2>
        <p class="p-heading p-large">Meat our team of specialized marketers and business developers which will help you research new products and launch them in new emerging markets</p>
      </div> <!-- end of col -->
    </div> <!-- end of row -->
    <div class="row">
      <div class="col-lg-12">

        <!-- Team Member -->
        <div class="team-member">
          <div class="image-wrapper">
            <img class="img-fluid" src="{{asset('frontpage_asset/images/team-member-1.svg')}}" alt="alternative">
          </div> <!-- end of image-wrapper -->
          <p class="p-large"><strong>Lacy Whitelong</strong></p>
          <p class="job-title">Business Developer</p>
          <span class="social-icons">
            <span class="fa-stack">
              <a href="#your-link">
                <i class="fas fa-circle fa-stack-2x facebook"></i>
                <i class="fab fa-facebook-f fa-stack-1x"></i>
              </a>
            </span>
            <span class="fa-stack">
              <a href="#your-link">
                <i class="fas fa-circle fa-stack-2x twitter"></i>
                <i class="fab fa-twitter fa-stack-1x"></i>
              </a>
            </span>
          </span> <!-- end of social-icons -->
        </div> <!-- end of team-member -->
        <!-- end of team member -->

        <!-- Team Member -->
        <div class="team-member">
          <div class="image-wrapper">
            <img class="img-fluid" src="{{asset('frontpage_asset/images/team-member-2.svg')}}" alt="alternative">
          </div> <!-- end of image wrapper -->
          <p class="p-large"><strong>Chris Brown</strong></p>
          <p class="job-title">Online Marketer</p>
          <span class="social-icons">
            <span class="fa-stack">
              <a href="#your-link">
                <i class="fas fa-circle fa-stack-2x facebook"></i>
                <i class="fab fa-facebook-f fa-stack-1x"></i>
              </a>
            </span>
            <span class="fa-stack">
              <a href="#your-link">
                <i class="fas fa-circle fa-stack-2x twitter"></i>
                <i class="fab fa-twitter fa-stack-1x"></i>
              </a>
            </span>
          </span> <!-- end of social-icons -->
        </div> <!-- end of team-member -->
        <!-- end of team member -->

        <!-- Team Member -->
        <div class="team-member">
          <div class="image-wrapper">
            <img class="img-fluid" src="{{asset('frontpage_asset/images/team-member-3.svg')}}" alt="alternative">
          </div> <!-- end of image wrapper -->
          <p class="p-large"><strong>Sheila Zimerman</strong></p>
          <p class="job-title">Software Engineer</p>
          <span class="social-icons">
            <span class="fa-stack">
              <a href="#your-link">
                <i class="fas fa-circle fa-stack-2x facebook"></i>
                <i class="fab fa-facebook-f fa-stack-1x"></i>
              </a>
            </span>
            <span class="fa-stack">
              <a href="#your-link">
                <i class="fas fa-circle fa-stack-2x twitter"></i>
                <i class="fab fa-twitter fa-stack-1x"></i>
              </a>
            </span>
          </span> <!-- end of social-icons -->
        </div> <!-- end of team-member -->
        <!-- end of team member -->

        <!-- Team Member -->
        <div class="team-member">
          <div class="image-wrapper">
            <img class="img-fluid" src="{{asset('frontpage_asset/images/team-member-4.svg')}}" alt="alternative">
          </div> <!-- end of image wrapper -->
          <p class="p-large"><strong>Mary Villalonga</strong></p>
          <p class="job-title">Product Manager</p>
          <span class="social-icons">
            <span class="fa-stack">
              <a href="#your-link">
                <i class="fas fa-circle fa-stack-2x facebook"></i>
                <i class="fab fa-facebook-f fa-stack-1x"></i>
              </a>
            </span>
            <span class="fa-stack">
              <a href="#your-link">
                <i class="fas fa-circle fa-stack-2x twitter"></i>
                <i class="fab fa-twitter fa-stack-1x"></i>
              </a>
            </span>
          </span> <!-- end of social-icons -->
        </div> <!-- end of team-member -->
        <!-- end of team member -->

      </div> <!-- end of col -->
    </div> <!-- end of row -->
  </div> <!-- end of container -->
</div> <!-- end of basic-4 -->
<!-- end of about -->

<!-- Footer -->
<div class="footer">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <div class="footer-col">
          <h4>About Evolo</h4>
          <p>We're passionate about offering some of the best business growth services for startups</p>
        </div>
      </div> <!-- end of col -->
      <div class="col-md-4">
        <div class="footer-col middle">
          <h4>Important Links</h4>
          <ul class="list-unstyled li-space-lg">
            <li class="media">
              <i class="fas fa-square"></i>
              <div class="media-body">Our business partners <a class="turquoise" href="#your-link">startupguide.com</a></div>
            </li>
            <li class="media">
              <i class="fas fa-square"></i>
              <div class="media-body">Read our <a class="turquoise" href="terms-conditions.html">Terms & Conditions</a>, <a class="turquoise" href="privacy-policy.html">Privacy Policy</a></div>
            </li>
          </ul>
        </div>
      </div> <!-- end of col -->
      <div class="col-md-4">
        <div class="footer-col last">
          <h4>Social Media</h4>
          <span class="fa-stack">
            <a href="#your-link">
              <i class="fas fa-circle fa-stack-2x"></i>
              <i class="fab fa-facebook-f fa-stack-1x"></i>
            </a>
          </span>
          <span class="fa-stack">
            <a href="#your-link">
              <i class="fas fa-circle fa-stack-2x"></i>
              <i class="fab fa-twitter fa-stack-1x"></i>
            </a>
          </span>
          <span class="fa-stack">
            <a href="#your-link">
              <i class="fas fa-circle fa-stack-2x"></i>
              <i class="fab fa-google-plus-g fa-stack-1x"></i>
            </a>
          </span>
          <span class="fa-stack">
            <a href="#your-link">
              <i class="fas fa-circle fa-stack-2x"></i>
              <i class="fab fa-instagram fa-stack-1x"></i>
            </a>
          </span>
          <span class="fa-stack">
            <a href="#your-link">
              <i class="fas fa-circle fa-stack-2x"></i>
              <i class="fab fa-linkedin-in fa-stack-1x"></i>
            </a>
          </span>
        </div>
      </div> <!-- end of col -->
    </div> <!-- end of row -->
  </div> <!-- end of container -->
</div> <!-- end of footer -->
<!-- end of footer -->

</body>