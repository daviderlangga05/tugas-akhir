@extends('frontpage.master')
<!-- Header -->


<header id="header" class="ex-header">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>Artikel</h1>
      </div> <!-- end of col -->
    </div> <!-- end of row -->
  </div> <!-- end of container -->
</header> <!-- end of ex-header -->
<!-- end of header -->


<!-- Breadcrumbs -->
<div class="ex-basic-1">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="breadcrumbs">
          <a href="/">Home</a><i class="fa fa-angle-double-right"></i>
          <span>Artikel</span>
          <i class="fa fa-angle-double-right"></i>
          <span>{{$query ->kategori->nama_kategori}}</span>
        </div> <!-- end of breadcrumbs -->
      </div> <!-- end of col -->
    </div> <!-- end of row -->
  </div> <!-- end of container -->
</div> <!-- end of ex-basic-1 -->
<!-- end of breadcrumbs -->


<!-- Privacy Content -->
<div class="ex-basic-2">
  <div class="container">
    <div class="row">
      <div class="col-lg-10 offset-lg-1">
        <div class="text-container">
          <h3>{{$query ->judul}} </h3>
          <p>Author : {{$query ->author->name}} || {{$query ->created_at}}</p>
          <img src="{{'/gambar_artikel/'.$query -> gambar}}">
          <p>{!!$query ->isi!!}</p>       

          <h2>Komentar :</h2>
          <div class="card card-primary">
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="/komentar" method="post">
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Nama</label>
                    <input type="text" class="form-control"  placeholder="ketik nama" name="nama_pengirim">
                    <input type="hidden" class="form-control"  placeholder="ketik nama" name="id_artikel" value="{{$query ->id}}">
                    @error('nama_pengirim')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                  </div>
                  <div class="form-group">
                        <label>Textarea</label>
                        <textarea class="form-control" rows="3" placeholder="Enter ..." name="isi"></textarea>
                        @error('isi')
                            <div class="alert alert-danger">{{ $message }}</div>
                        @enderror
                      </div>
                 
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Submit</button>
                </div>
              </form>
            </div>
            <br> </br>

            <h2>Daftar Komentar </h2>
            <div class="container" style="border: 1px solid #c4d8dc; border-radius: 0.5rem; margin-bottom:30px">
            @forelse($queryKomentar as $key => $qKomentar )
            <div class="text-container">
                      <h4>Pengirim : {{$qKomentar -> nama_pengirim}}</h4>
                      {{$qKomentar -> isi}} <hr>
                    </div> <!-- end of text-container -->  
                    @empty <p>Belum ada koementar masuk </p>
            @endforelse
            </div>
           
            

  </div> <!-- end of container -->
</div> <!-- end of ex-basic-2 -->
<!-- end of privacy content -->




<!-- Footer -->
<div class="footer">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <div class="footer-col">
          <h4>About Evolo</h4>
          <p>We're passionate about offering the best human resource services to our customers</p>
        </div>
      </div> <!-- end of col -->
      <div class="col-md-4">
        <div class="footer-col middle">
          <h4>Important Links</h4>
          <ul class="list-unstyled li-space-lg">
            <li class="media">
              <i class="fas fa-square"></i>
              <div class="media-body">Our business partners <a class="turquoise" href="#your-link">startupguide.com</a></div>
            </li>
            <li class="media">
              <i class="fas fa-square"></i>
              <div class="media-body">Read our <a class="turquoise" href="terms-conditions.html">Terms & Conditions</a>, <a class="turquoise" href="privacy-policy.html">Privacy Policy</a></div>
            </li>
          </ul>
        </div>
      </div> <!-- end of col -->
      <div class="col-md-4">
        <div class="footer-col last">
          <h4>Social Media</h4>
          <span class="fa-stack fa-lg">
            <a href="#your-link">
              <i class="fas fa-circle fa-stack-2x"></i>
              <i class="fab fa-facebook-f fa-stack-1x"></i>
            </a>
          </span>
          <span class="fa-stack fa-lg">
            <a href="#your-link">
              <i class="fas fa-circle fa-stack-2x"></i>
              <i class="fab fa-twitter fa-stack-1x"></i>
            </a>
          </span>
          <span class="fa-stack fa-lg">
            <a href="#your-link">
              <i class="fas fa-circle fa-stack-2x"></i>
              <i class="fab fa-google-plus-g fa-stack-1x"></i>
            </a>
          </span>
          <span class="fa-stack fa-lg">
            <a href="#your-link">
              <i class="fas fa-circle fa-stack-2x"></i>
              <i class="fab fa-instagram fa-stack-1x"></i>
            </a>
          </span>
          <span class="fa-stack fa-lg">
            <a href="#your-link">
              <i class="fas fa-circle fa-stack-2x"></i>
              <i class="fab fa-linkedin-in fa-stack-1x"></i>
            </a>
          </span>
        </div>
      </div> <!-- end of col -->
    </div> <!-- end of row -->
  </div> <!-- end of container -->
</div> <!-- end of footer -->
<!-- end of footer -->