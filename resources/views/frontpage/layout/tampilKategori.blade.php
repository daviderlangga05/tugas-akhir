@extends('frontpage.master')
<!-- Header -->


<header id="header" class="ex-header">
  <div class="container">
    <div class="row">
      <div class="col-md-12">
        <h1>{{$queryidKategori->nama_kategori}}</h1>
      </div> <!-- end of col -->
    </div> <!-- end of row -->
  </div> <!-- end of container -->
</header> <!-- end of ex-header -->
<!-- end of header -->


<!-- Breadcrumbs -->
<div class="ex-basic-1">
  <div class="container">
    <div class="row">
      <div class="col-lg-12">
        <div class="breadcrumbs">
          <a href="/">Home</a><i class="fa fa-angle-double-right"></i>
          <span>Artikel</span>
          <i class="fa fa-angle-double-right"></i>
          <span>{{$queryidKategori->nama_kategori}}</span>
        </div> <!-- end of breadcrumbs -->
      </div> <!-- end of col -->
    </div> <!-- end of row -->
  </div> <!-- end of container -->
</div> <!-- end of ex-basic-1 -->
<!-- end of breadcrumbs -->


<!-- Privacy Content -->
<div class="ex-basic-2">
  <div class="container">
    <div class="row">
      <div class="col-lg-10 offset-lg-1">
        <div class="text-container">
        @forelse($queryArtikel as $key => $artikel )
        <div class="container" style="border: 1px solid #c4d8dc; border-radius: 0.5rem; ">
        <div class="text-container" style="padding:12px">
          <h3>{{$artikel -> judul}}</h3>
          <img src ="../../../../gambar_artikel/{{$artikel -> gambar}}" width="200px" 
          style="float:left; padding-right:20px;
            padding-bottom:px;
            padding-left:5px;">
          <p>{!!Str::limit ($artikel -> isi,250, ' ....')!!}</p> 
          <a href="/artikel/detail/{{$artikel -> id}}/{{$artikel->kategori->link_seo}}/{{$artikel -> link_seo}}"> <i>lihat Selengkapnya</i> </a>
          <hr>
          <i>Author : {{$artikel ->author->name}} || {{$artikel -> created_at}}</i>
        </div> <!-- end of text-container -->  
</div>
@empty <p>No Data </p>
@endforelse

  </div> <!-- end of container -->
</div> <!-- end of ex-basic-2 -->
<!-- end of privacy content -->


<!-- Footer -->
<div class="footer">
  <div class="container">
    <div class="row">
      <div class="col-md-4">
        <div class="footer-col">
          <h4>About Evolo</h4>
          <p>We're passionate about offering the best human resource services to our customers</p>
        </div>
      </div> <!-- end of col -->
      <div class="col-md-4">
        <div class="footer-col middle">
          <h4>Important Links</h4>
          <ul class="list-unstyled li-space-lg">
            <li class="media">
              <i class="fas fa-square"></i>
              <div class="media-body">Our business partners <a class="turquoise" href="#your-link">startupguide.com</a></div>
            </li>
            <li class="media">
              <i class="fas fa-square"></i>
              <div class="media-body">Read our <a class="turquoise" href="terms-conditions.html">Terms & Conditions</a>, <a class="turquoise" href="privacy-policy.html">Privacy Policy</a></div>
            </li>
          </ul>
        </div>
      </div> <!-- end of col -->
      <div class="col-md-4">
        <div class="footer-col last">
          <h4>Social Media</h4>
          <span class="fa-stack fa-lg">
            <a href="#your-link">
              <i class="fas fa-circle fa-stack-2x"></i>
              <i class="fab fa-facebook-f fa-stack-1x"></i>
            </a>
          </span>
          <span class="fa-stack fa-lg">
            <a href="#your-link">
              <i class="fas fa-circle fa-stack-2x"></i>
              <i class="fab fa-twitter fa-stack-1x"></i>
            </a>
          </span>
          <span class="fa-stack fa-lg">
            <a href="#your-link">
              <i class="fas fa-circle fa-stack-2x"></i>
              <i class="fab fa-google-plus-g fa-stack-1x"></i>
            </a>
          </span>
          <span class="fa-stack fa-lg">
            <a href="#your-link">
              <i class="fas fa-circle fa-stack-2x"></i>
              <i class="fab fa-instagram fa-stack-1x"></i>
            </a>
          </span>
          <span class="fa-stack fa-lg">
            <a href="#your-link">
              <i class="fas fa-circle fa-stack-2x"></i>
              <i class="fab fa-linkedin-in fa-stack-1x"></i>
            </a>
          </span>
        </div>
      </div> <!-- end of col -->
    </div> <!-- end of row -->
  </div> <!-- end of container -->
</div> <!-- end of footer -->
<!-- end of footer -->