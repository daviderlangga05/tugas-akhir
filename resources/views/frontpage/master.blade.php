<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- SEO Meta Tags -->
  <meta name="description" content="Create a stylish landing page for your business startup and get leads for the offered services with this HTML landing page template.">
  <meta name="author" content="Inovatik">

  <!-- OG Meta Tags to improve the way the post looks when you share the page on LinkedIn, Facebook, Google+ -->
  <meta property="og:site_name" content="" /> <!-- website name -->
  <meta property="og:site" content="" /> <!-- website link -->
  <meta property="og:title" content="" /> <!-- title shown in the actual shared post -->
  <meta property="og:description" content="" /> <!-- description shown in the actual shared post -->
  <meta property="og:image" content="" /> <!-- image link, make sure it's jpg -->
  <meta property="og:url" content="" /> <!-- where do you want your post to link to -->
  <meta property="og:type" content="article" />

  <!-- Website Title -->
  <title>Blogger Milenial</title>

  <!-- Styles -->
  <link href="https://fonts.googleapis.com/css?family=Raleway:400,400i,600,700,700i&amp;subset=latin-ext" rel="stylesheet">
  <link href="{{asset('frontpage_asset/css/bootstrap.css')}}" rel="stylesheet">
  <link href="{{asset('frontpage_asset/css/fontawesome-all.css')}}" rel="stylesheet">
  <link href="{{asset('frontpage_asset/css/swiper.css')}}" rel="stylesheet">
  <link href="{{asset('frontpage_asset/css/magnific-popup.css')}}" rel="stylesheet">
  <link href="{{asset('frontpage_asset/css/styles.css')}}" rel="stylesheet">
  @stack('Ekko-Lightbox')

  <!-- Favicon  -->
  <link rel="icon" href="{{asset('front_page/images/favicon.png')}}">
</head>

<body data-spy="scroll" data-target=".fixed-top">

  <!-- Preloader -->
  <div class="spinner-wrapper">
    <div class="spinner">
      <div class="bounce1"></div>
      <div class="bounce2"></div>
      <div class="bounce3"></div>
    </div>
  </div>
  <!-- end of preloader -->


  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark navbar-custom fixed-top">
    <!-- Text Logo - Use this if you don't have a graphic logo -->
    <!-- <a class="navbar-brand logo-text page-scroll" href="index.html">Evolo</a> -->

    <!-- Image Logo -->
    <a class="navbar-brand logo-image" href="#"><img src="{{asset('frontpage_asset/images/logo.svg')}}" alt="alternative"></a>

    <!-- Mobile Menu Toggle Button -->
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-awesome fas fa-bars"></span>
      <span class="navbar-toggler-awesome fas fa-times"></span>
    </button>
    <!-- end of mobile menu toggle button -->

    <div class="collapse navbar-collapse" id="navbarsExampleDefault">
      <ul class="navbar-nav ml-auto">
        <li class="nav-item">
          <a class="nav-link page-scroll" href="/">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link page-scroll" href="#kategori">Artikel</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle page-scroll" href="#about" id="navbarDropdown" role="button" aria-haspopup="true" aria-expanded="false">Kategori</a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdown">
          @foreach($queryKategori as $key => $kategori )
            <a class="dropdown-item" href="/artikel/kategori/{{$kategori -> id}}/{{$kategori -> link_seo}}"><span class="item-text">{{$kategori -> nama_kategori}}</span></a>
            <div class="dropdown-items-divide-hr"></div>
            @endforeach
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link page-scroll" href="/galeri">Gallery</a>
        </li>
      </ul>
    </div>
  </nav> <!-- end of navbar -->
  <!-- end of navigation -->

  @yield('content')
  <!-- Copyright -->
  <div class="copyright">
    <div class="container">
      <div class="row">
        <div class="col-lg-12">
          <p class="p-small">Copyright © 2020 <a href="https://inovatik.com">Inovatik</a> - All rights reserved</p>
        </div> <!-- end of col -->
      </div> <!-- enf of row -->
    </div> <!-- end of container -->
  </div> <!-- end of copyright -->
  <!-- end of copyright -->


  <!-- Scripts -->
  <script src="{{asset('frontpage_asset/js/jquery.min.js')}}"></script> <!-- jQuery for Bootstrap's JavaScript plugins -->
  <script src="{{asset('frontpage_asset/js/popper.min.js')}}"></script> <!-- Popper tooltip library for Bootstrap -->
  <script src="{{asset('frontpage_asset/js/bootstrap.min.js')}}"></script> <!-- Bootstrap framework -->
  <script src="{{asset('frontpage_asset/js/jquery.easing.min.js')}}"></script> <!-- jQuery Easing for smooth scrolling between anchors -->
  <script src="{{asset('frontpage_asset/js/swiper.min.js')}}"></script> <!-- Swiper for image and text sliders -->
  <script src="{{asset('frontpage_asset/js/jquery.magnific-popup.js')}}"></script> <!-- Magnific Popup for lightboxes -->
  <script src="{{asset('frontpage_asset/js/validator.min.js')}}"></script> <!-- Validator.js - Bootstrap plugin that validates forms -->
  <script src="{{asset('frontpage_asset/js/scripts.js')}}"></script> <!-- Custom scripts -->
  @stack('script-Ekko-Lightbox')
</body>

</html>