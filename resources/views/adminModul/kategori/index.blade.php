@extends('adminpage.master')
@section('content')
<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Kategori</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/admin">Home</a></li>
              <li class="breadcrumb-item active">Artikel</li>
            </ol>
          </div>
        </div>
      </div>
</section>
<div class="container-fluid">
      @if(session('success'))
          <div class="alert alert-success">
            {{ session('success') }}
          </div>
       @endif

        <a href="#kategori" data-toggle="modal" class="btn btn-primary">Create</a>
</div>

<!--modal-->
<div class="modal fade" id="kategori" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

    <form role="form" action="/admin/kategori" method="POST">
      @csrf
        <div class="modal-header">
          <h5 class="modal-title">Upload Kategori</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
          <label for="namaKategori">Nama Kategori</label>
          <input type="text" class="form-control" id="namaKategori" name="namaKategori"  aria-describedby="namaKategori" placeholder="Masukkan kategori">
          <small id="namaKategori" class="form-text text-muted">Kategorikan blogmu</small>
          @error('namaKategori')
              <div class="alert alert-danger">{{ $message }}</div>
          @enderror

        </div>

        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Save</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
</div><br>

<div class="container-fluid">
<table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Nama Kategori</th>
      <th scope="col">Link SEO</th>
      <th scope="col">Opsi</th>
    </tr>
  </thead>
  <tbody>
  @forelse( $kategori as $key => $kategoriModel)
    <tr>
        <td> {{ $key + 1 }} </td>
        <td> {{ $kategoriModel -> nama_kategori }} </td>
        <td> {{ $kategoriModel -> link_seo }}</td>
        <td style="display: flex;">
          <a href="#editKategori{{ $kategoriModel->id }}" data-toggle="modal" class="btn btn-default btn-sm">Edit</a>
          
          <form action="{{route('kategori.destroy',['kategori' => $kategoriModel -> id])}}" method="post">
              @csrf
              @method('DELETE')
              <input type="submit" value="delete" class="btn btn-danger btn-sm">
          </form>
          
          <!-- Edit modal -->
          <div class="modal fade" id="editKategori{{ $kategoriModel->id }}" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
              <div class="modal-content">

              <form role="form" action="{{route('kategori.update',['kategori' => $kategoriModel -> id])}}" method="POST">
                @csrf
                @method('PUT')
                  <div class="modal-header">
                    <h5 class="modal-title">Edit Kategori</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div> 

                  <div class="modal-body">
                    <label for="namaKategori">Nama Kategori</label>
                    <input type="text" class="form-control" id="namaKategori" name="namaKategori"  aria-describedby="namaKategori" 
                    placeholder="Masukkan kategori" value="{{ $kategoriModel -> nama_kategori }}">
                    <small id="namaKategori" class="form-text text-muted">Kategorikan blogmu</small>
                    @error('namaKategori')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                    
                  </div>

                  <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <!-- end edit modal -->
        </td>
    </tr>

    @empty
      <tr>
        <td colspan="5" align="center">No Post</td>
      </tr>
    @endforelse
  </tbody>
</table>
</div>
@endsection