@extends('adminpage.master')
@section('content')
<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Komentar</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Artikel</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <section class="content">
@if (session('berhasil'))
<div class="alert alert-success">
{{session('berhasil')}}
</div>
@endif
<!--<a class= "btn btn-primary mb-2" href="/artikel/create">Create</a>-->
<table class="table table-bordered">
  <thead>                  
    <tr>
      <th style="width: 10px">No</th>
      <th>Nama Pengirim</th>
      <th>Isi</th>
      <th>Judul Artikel</th>
      <th style="width: 40px">Opsi</th>
    </tr>
  </thead>
  <tbody>
      @forelse($komentar as $key => $query )
     
    <tr>
      <td>{{$key + 1}}</td>
      <td>{{$query -> nama_pengirim}}</td>
      <td>{{$query -> isi}}</td>
      <td>{{$query ->artikelkomen->judul}}</td>
      <td style="display: flex;">
      <form action="{{route('komentarArtikel.destroy',['komentarArtikel' => $query -> id])}}" method="post">
      @csrf
      @method('Delete')
      <input type="submit" class= "btn btn-danger btn-sm" value="Del">  </form>
      </td>
      
    </tr>
    @empty <p>No Data </p>
    @endforelse
    
  </tbody>
</table>
@endsection