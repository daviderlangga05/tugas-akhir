@extends('adminpage.master')

@section('content')
<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Galeri Foto</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="/admin">Home</a></li>
              <li class="breadcrumb-item active">Artikel</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
</section>
<div class="container-fluid">
      @if(session('success'))
        <div class="alert alert-success">
          {{ session('success') }}
        </div>
      @endif
        <div class="col-md-2 col-md">
            <a href="#uploadFoto" data-toggle="modal" class="btn btn-primary">Upload Foto</a>
        </div>
</div>

<!-- modal -->
<div class="modal fade" id="uploadFoto" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">

    <form role="form" action="/admin/foto" method="POST" enctype="multipart/form-data">
              @csrf>
      @csrf
        <div class="modal-header">
          <h5 class="modal-title">Upload Foto</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>

        <div class="modal-body">
          <label for="judulFoto">Judul Foto</label>
          <input type="text" class="form-control" id="judulFoto" name="judulFoto" aria-describedby="judulFoto" placeholder="Masukkan judul foto">
          <small id="judulFoto" class="form-text text-muted">Buat judul fotomu sekreatif mungkin</small>
          @error('judulFoto')
              <div class="alert alert-danger">{{ $message }}</div>
          @enderror
          <br>
          <div class="form-group">
              <label for="fileInput">Foto input here</label>
              <input type="file" class="form-control-file" id="fileInput" name="fileInput">
          </div>
          @error('fileInput')
              <div class="alert alert-danger">{{ $message }}</div>
          @enderror

          <label for="status">status Publikasi</label>
            <select class="form-control" id="status" name="status">
              <option value="YA">YA</option>
              <option value="TIDAK">TIDAK</option>
            </select>
        </div>
        <div class="modal-footer">
          <button type="submit" class="btn btn-primary">Save</button>
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </form>
    </div>
  </div>
</div><br>
<!-- end modal -->

<!-- table data foto -->
<div class="container-fluid">
<table class="table table-bordered">
  <thead>
    <tr>
      <th scope="col">#</th>
      <th scope="col">Judul Foto</th>
      <th scope="col">Foto</th>
      <th scope="col">Status</th>
      <th scope="col">opsi</th>
    </tr>
  </thead>
  <tbody>
    @forelse( $fotoModel as $key => $fotoModel)
    <tr>
        <td> {{ $key + 1 }} </td>
        <td> {{ $fotoModel -> judul_foto }} </td>
        <td><a href="{{'../galeri_foto/'.$fotoModel -> foto}}"> {{ $fotoModel -> foto }} </a></td>
        <td> {{ $fotoModel -> status }} </td>
        <td style="display: flex;">
          <a href="#editFoto{{ $fotoModel->id }}" data-toggle="modal" class="btn btn-default btn-sm">Edit</a>
          
          <form action="{{route('foto.destroy',['foto' => $fotoModel -> id])}}" method="post">
              @csrf
              @method('DELETE')
              <input type="submit" value="delete" class="btn btn-danger btn-sm">
          </form>
          

          <!-- Edit modal -->
          <div class="modal fade" id="editFoto{{ $fotoModel->id }}" tabindex="-1" role="dialog">
            <div class="modal-dialog" role="document">
              <div class="modal-content">

              <form role="form" action="/admin/foto/{{$fotoModel -> id}}" method="POST" enctype="multipart/form-data">
              @csrf>
                @csrf
                @method('PUT')
                  <div class="modal-header">
                    <h5 class="modal-title">Edit Foto</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                      <span aria-hidden="true">&times;</span>
                    </button>
                  </div> 

                  <div class="modal-body">
                    <label for="judulFoto">Judul Foto</label>
                    <input type="text" class="form-control" id="judulFoto" name="judulFoto" aria-describedby="judulFoto" placeholder="Masukkan judul foto" value="{{ $fotoModel -> judul_foto }}">
                    @error('judulFoto')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror
                    <br>
                    <div class="form-group">
                        <label for="fileInput">Foto input here</label>
                        <input type="file" class="form-control-file" id="fileInput" name="fileInput">
                        {{ $fotoModel -> foto }}
                    </div>
                    @error('fileInput')
                        <div class="alert alert-danger">{{ $message }}</div>
                    @enderror

                    <label for="status">status Publikasi</label>
                      <select class="form-control" id="status" name="status">
                        <option value="YA">YA</option>
                        <option value="TIDAK">TIDAK</option>
                      </select>
                  </div>
                  <div class="modal-footer">
                    <button type="submit" class="btn btn-primary">Simpan</button>
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Keluar</button>
                  </div>
                </form>
              </div>
            </div>
          </div>
          <!-- end edit modal -->
        </td>
    </tr>

    @empty
      <tr>
        <td colspan="5" align="center">No Post</td>
      </tr>
    @endforelse
  </tbody>
</table>
</div>


 @endsection