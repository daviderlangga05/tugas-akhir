@extends('adminpage.master')
@section('content')
<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Artikel</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Artikel</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <section class="content">
@if (session('berhasil'))
<div class="alert alert-success">
{{session('berhasil')}}
</div>
@endif
<!--<a class= "btn btn-primary mb-2" href="/artikel/create">Create</a>-->
<a class= "btn btn-primary mb-2" href="{{route('artikel.create')}}">Create</a>
<table class="table table-bordered">
  <thead>                  
    <tr>
      <th style="width: 10px">No</th>
      <th>Judul</th>
      <th>Isi</th>
      <th>Kategori</th>
      <th>Gambar</th>
      <th>Penulis</th>
      <th>Tanggal/Jam</th>
      <th style="width: 40px">Opsi</th>
    </tr>
  </thead>
  <tbody>
      @forelse($query as $key => $query )
     
    <tr>
      <td>{{$key + 1}}</td>
      <td>{{$query -> judul}}</td>
      <td>{!!Str::limit ($query -> isi,100, ' ....')!!}</td>
      <td>{{$query ->kategori->nama_kategori}}</td>
      <td><a href="{{'../gambar_artikel/'.$query -> gambar}}">{{$query -> gambar}}</a></td>
      <td>{{$query->author->name}}</td>
      <td>{{$query -> created_at}}</td>
      <td style="display: flex;">
      <a class= "btn btn-default btn-sm" href="{{route('artikel.edit',['artikel' => $query -> id],'edit')}}">edit</a>
      <form action="{{route('artikel.destroy',['artikel' => $query -> id])}}" method="post">
      @csrf
      @method('Delete')
      <input type="submit" class= "btn btn-danger btn-sm" value="Del">  </form>
      </td>
      
    </tr>
    @empty <p>No Data </p>
    @endforelse
    
  </tbody>
</table>
@endsection