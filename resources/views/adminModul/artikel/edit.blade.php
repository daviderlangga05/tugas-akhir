@extends('adminpage.master')
@push('textEditor-tinyMCE')
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
@endpush
@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Artikel</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Artikel</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <section class="content">
    <div class="col-md-12">
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Edit Artikel</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{route('artikel.update',['artikel' => $query -> id])}}" method="post" enctype="multipart/form-data">
              @csrf
              @method('PUT')
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Judul</label>
                    <input class="form-control" name="judul" value="{{old('judul',$query ->judul)}}">
                  </div>
                  @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                  <div class="form-group">
                  <label>Isi</label>
                  <textarea class="form-control my-editor" name="isi">{!! old('isi',$query ->isi) !!}
                  </textarea>
                      </div>
                      @error('isi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                  
                <div class="form-group">
                        <label for="exampleFormControlFile1">Tambah gambar</label>
                        <input type="file" class="form-control-file" id="exampleFormControlFile1" name="gambar">
                        <a href="{{'../../../gambar_artikel/'.$query ->gambar}}" target="_blank">{{$query ->gambar}}</a>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">status Publikasi</label>
                    <select class="form-control" id="exampleFormControlSelect1" name="status">
                    <option value="{{$query ->status}}">{{$query ->status}}</option>
                    <option value="YA">YA</option>
                    <option value="TIDAK">TIDAK</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Kategori</label>
                    <select class="form-control" id="exampleFormControlSelect1" name="kategori">
                    <option value="{{$query -> id_kategori}}">{{$query ->kategori->nama_kategori}}</option>
                    @foreach($queryKategori as $key => $queryKategori )
                    <option value="{{$query -> id}}">{{$queryKategori -> nama_kategori}}</option>
                    @endforeach
                    
                    </select>
                </div>


                <div class="form-group">
                  <label for="exampleInputEmail1">tags</label>
                    <input class="form-control" id="tags" name="tags" value="{{old('tags')}}"
                  placeholder ="Pisahkan dengan koma, contoh :postingan,beritaterkini,update">
                 tags : @forelse ($query->tags as $tag)
                  #{{$tag->tag_name}}
                    @empty
                    No Tags
                  @endforelse
                  <br>*Jika tidak diubah lewatkan saja
                  </div>  
                </div>
            



                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
              </form>
            </div>
</div>


</section>


@endsection

@push('script-textEditor')
<script>
  var editor_config = {
    path_absolute : "/",
    selector: "textarea.my-editor",
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
</script>
@endpush()