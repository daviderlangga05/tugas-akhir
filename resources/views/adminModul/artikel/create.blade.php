@extends('adminpage.master')
@push('textEditor-tinyMCE')
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
@endpush
@section('content')

<section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Data Artikel</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Artikel</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>
    <section class="content">
    <div class="col-md-12">
<div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add Artikel</h3>
              </div>
              <!-- /.card-header -->
              <!-- form start -->
              <form role="form" action="{{route('artikel.store')}} " method="post" enctype="multipart/form-data">
              @csrf
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Judul</label>
                    <input class="form-control" name="judul" value="{{old('judul','')}}">
                  </div>
                  @error('judul')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                  <div class="form-group">
                  <label>Isi</label>
                  <textarea class="form-control my-editor" name="isi">{!! old('isi','') !!}

                  </textarea>
                      </div>
                      @error('isi')
                    <div class="alert alert-danger">
                        {{ $message }}
                    </div>
                @enderror
                  
                <div class="form-group">
                        <label for="exampleFormControlFile1">Tambah gambar</label>
                        <input type="file" class="form-control-file" id="exampleFormControlFile1" name="gambar">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">status Publikasi</label>
                    <select class="form-control" id="exampleFormControlSelect1" name="status">
                    <option value="YA">YA</option>
                    <option value="TIDAK">TIDAK</option>
                    </select>
                </div>
                <div class="form-group">
                    <label for="exampleFormControlSelect1">Kategori</label>
                    <select class="form-control" id="exampleFormControlSelect1" name="kategori">
                    @foreach($query as $key => $query )
                    <option value="{{$query -> id}}">{{$query -> nama_kategori}}</option>
                    @endforeach
                    
                    </select>
                </div>



                <div class="form-group">
                  <label for="exampleInputEmail1">tags</label>
                    <input class="form-control" id="tags" name="tags" value="{{old('tags','')}}"
                  placeholder ="Pisahkan dengan koma, contoh :postingan,beritaterkini,update">

                  </div>  
                </div>
                <!-- /.card-body -->

                <div class="card-footer">
                  <button type="submit" class="btn btn-primary">Simpan</button>
                </div>
              </form>
            </div>
</div>


</section>


@endsection

@push('script-textEditor')
<script>
  var editor_config = {
    path_absolute : "/",
    selector: "textarea.my-editor",
    plugins: [
      "advlist autolink lists link image charmap print preview hr anchor pagebreak",
      "searchreplace wordcount visualblocks visualchars code fullscreen",
      "insertdatetime media nonbreaking save table contextmenu directionality",
      "emoticons template paste textcolor colorpicker textpattern"
    ],
    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image media",
    relative_urls: false,
    file_browser_callback : function(field_name, url, type, win) {
      var x = window.innerWidth || document.documentElement.clientWidth || document.getElementsByTagName('body')[0].clientWidth;
      var y = window.innerHeight|| document.documentElement.clientHeight|| document.getElementsByTagName('body')[0].clientHeight;

      var cmsURL = editor_config.path_absolute + 'laravel-filemanager?field_name=' + field_name;
      if (type == 'image') {
        cmsURL = cmsURL + "&type=Images";
      } else {
        cmsURL = cmsURL + "&type=Files";
      }

      tinyMCE.activeEditor.windowManager.open({
        file : cmsURL,
        title : 'Filemanager',
        width : x * 0.8,
        height : y * 0.8,
        resizable : "yes",
        close_previous : "no"
      });
    }
  };

  tinymce.init(editor_config);
</script>
@endpush()