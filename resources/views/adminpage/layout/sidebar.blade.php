<aside class="main-sidebar sidebar-dark-primary elevation-4">
  <!-- Brand Logo -->
  <a href="../../index3.html" class="brand-link">
    <img src="{{asset('adminpage_asset/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
    <span class="brand-text font-weight-light">Tugas Akhir</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="{{asset('adminpage_asset/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block">{{auth()->user()->name}}</a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->

        <li class="nav-item">
          <a href="{{route('artikel.index')}}" class="nav-link">
            <i class="nav-icon fas fa-info-circle"></i>
            <p>
              Artikel
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{route('kategori.index')}}" class="nav-link">
            <i class="nav-icon fas fa-info-circle"></i>
            <p>
              Kategori
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{route('komentarArtikel.index')}}" class="nav-link">
            <i class="nav-icon fas fa-info-circle"></i>
            <p>
              Komentar Artikel
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{route('foto.index')}}" class="nav-link">
            <i class="nav-icon fas fa-info-circle"></i>
            <p>
              Galeri Foto
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{route('iklan.index')}}" class="nav-link">
            <i class="nav-icon fas fa-info-circle"></i>
            <p>
              Iklan
            </p>
          </a>
        </li>
        <li class="nav-item">
          <a href="{{route('user.index')}}" class="nav-link">
            <i class="nav-icon fas fa-info-circle"></i>
            <p>
              Manajemen User/Penulis/Admin
            </p>
          </a>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>