<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\fotoModel;

use Illuminate\Support\faceades\Storage;
USE Auth;
class fotoController extends Controller
{
    public function index (){
        $fotoModel = fotoModel::all();
        return view ('adminModul.galeriFoto.index', compact('fotoModel'));
    }

    public function store( Request $request ){
        // dd($request->all());
        $request->validate([
            'judulFoto' => 'required',
            'fileInput' => 'required|image|max:2048' 
        ]);
        $gbrUpload = request()->file('fileInput');
        $gbrName = time().'.'. $gbrUpload->getClientOriginalExtension();
        $gbrPath = public_path('/galeri_foto/');
        $gbrUpload->move($gbrPath, $gbrName);
         $query = fotoModel::create([
             'judul_foto' => $request->judulFoto,
             'foto' => $gbrName,
             'status' => $request->status,
             'id_user' => Auth:: id()
         ]);

        
        return redirect('/admin/foto')->with('success', 'Foto berhasil di-upload');
    }


    public function update($id, Request $request){
        if($request->file('fileInput') == "")
        {
            $update = fotoModel::where('id', $id)->update([
             'judul_foto' => $request->judulFoto,
             'status' => $request->status,
             'id_user' => Auth:: id()
                ]);
        } else {
            $data = \App\fotoModel::findOrFail($id);
            unlink('galeri_foto/'.$data->fileInput);
            $gbrUpload = request()->file('fileInput');
            $gbrName = time().'.'. $gbrUpload->getClientOriginalExtension();
            $gbrPath = public_path('/galeri_foto/');
            $gbrUpload->move($gbrPath, $gbrName);
             $query = fotoModel::create([
                 'judul_foto' => $request->judulFoto,
                 'foto' => $gbrName,
                 'status' => $request->status,
                 'id_user' => Auth:: id()
             ]);
        }
        return redirect('/admin/foto')->with('success', 'Foto berhasil di-update');
    }

    public function destroy($id){
        $data = \App\fotoModel::findOrFail($id);
        unlink('galeri_foto/'.$data->fileInput);
        fotoModel::destroy($id);
        
        return redirect('/admin/foto')->with('success', 'Foto berhasil dihapus!');
    }


}
