<?php

namespace App\Http\Controllers\FrontEnd;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
USE DB;
USE Auth;
use App\kategoriModel;
use App\artikelModel;
use App\fotoModel;
use App\komentarModel;

class HomeController extends Controller
{
    public function index (){
        $queryKategori = kategoriModel::all();
        $queryArtikel = artikelModel::where("status",'YA')->get();

        return view('frontpage.layout.home',compact('queryKategori','queryArtikel'));
    }

    public function artikel($idArtikel){
       
        $query = artikelModel :: find($idArtikel);
        $queryKategori = kategoriModel::all();
        $queryKomentar = komentarModel::where("id_artikel",$idArtikel)->get();
        return view ('frontpage.layout.tampilArtikel',compact('query','queryKategori','queryKomentar'));
    }
    public function kategori($idKategori){
       
        $queryArtikel = artikelModel::where("id_kategori",$idKategori and "status",'YA' )->get();
        $queryidKategori = kategoriModel :: find($idKategori);
        $queryKategori = kategoriModel::all();
        return view ('frontpage.layout.tampilKategori',compact('queryArtikel','queryidKategori', 'queryKategori'));
    }
    public function galeri(){
        $queryKategori = kategoriModel::all();
        $queryfoto = fotoModel::where("status",'YA')->get();
        return view ('frontpage.layout.galeri',compact( 'queryKategori','queryfoto'));
    }

    public function store (Request $request){
            $request->validate([
                'nama_pengirim' => 'required|unique:komentar_artikel',
                'isi' => 'required'
            ]);
    

            $query = komentarModel::create([
              
                'nama_pengirim' => $request->nama_pengirim,
                'isi' => $request->isi,
                'id_artikel' =>  $request->id_artikel,
            ]);
        return redirect ()->back();
    }

}
