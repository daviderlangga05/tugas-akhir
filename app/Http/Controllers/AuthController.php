<?php

namespace App\Http\Controllers;

use Auth;
use App\user;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth as FacadesAuth;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;

class AuthController extends Controller
{
    public function login()
    {
        return view('loginpage.master');
    }

    public function postlogin(Request $request)
    {
        if (Auth::attempt($request->only('name', 'password'))) {
            return redirect('/admin');
        }
        return redirect('/login');
    }

    public function logout()
    {
        Auth::logout();
        return redirect('/');
    }

    public function signup(Request $request)
    {
        $user = new \App\User();
        $user->name = $request->name;
        $user->email = $request->email;
        $user->password = bcrypt($request->password);
        $user->remember_token = str_random(60);
        $user->save();
        return redirect('/login');
    }
}
