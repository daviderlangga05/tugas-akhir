<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\kategoriModel;
use Illuminate\Support\str;

class kategoriController extends Controller
{
    // public function index (){
    //     return view ('adminModul.kategori.index');
    // }

    public function store( Request $request ){
        // dd($request->all());
        $request->validate([
            'namaKategori' => 'required'
        ]);

        $kategoriModel = new kategoriModel;
        $kategoriModel->nama_kategori = $request["namaKategori"];
        $kategoriModel->link_seo =  Str::slug ($request["namaKategori"]);
        $kategoriModel->save();

        return redirect('/admin/kategori')->with('success', 'Kategori berhasil di-upload');
    }

    // public function edit($id){
    //     $kategoriModel = kategoriModel::find($id);
    //     return view ('adminModul.kategori.index');
    // }

    public function index(){
        $kategori = kategoriModel::all();
        return view ('adminModul.kategori.index', compact('kategori'));
    }

    public function destroy($idKategori){
        
        //orm
        kategoriModel::destroy($idKategori);    
        return redirect ('/admin/kategori')->with('success','Berhasil diHapus!');

    }

    public function update($idKategori, Request $request){
        $update = kategoriModel::where('id', $idKategori)->update([
            "nama_kategori" => $request["namaKategori"],
            "link_seo" =>  Str::slug ($request["namaKategori"])
        ]);

        return redirect('/admin/kategori')->with('success', 'Berhasil di-update');
    }

}
