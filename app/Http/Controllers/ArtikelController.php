<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\str;
use Illuminate\Support\faceades\Storage;
USE DB;
USE Auth;
use App\artikelModel;
use App\kategoriModel;
use App\tag;

class ArtikelController extends Controller
{
    public function index (){
        $query = artikelModel::all();
        return view ('adminModul.artikel.index',compact('query'));
    }

public function create (){
        $query = kategoriModel::all();
        return view ('adminModul.artikel.create',compact('query'));
    }
public function store (Request $request){
        $request->validate([
            'judul' => 'required|unique:artikel',
            'isi' => 'required',
            'gambar'         =>  'image|max:2048',
        ]);

        $tags_arr = explode(',', $request["tags"]);

        $tag_ids =[];
        foreach($tags_arr as $tag_name){
                $tag = Tag::where ("tag_name", $tag_name)->first();
                if ($tag){
                    $tag_ids[] = $tag->id;
                }else{
                    $new_tag = Tag::create(["tag_name" =>$tag_name]);
                    $tag_ids[] = $new_tag->id;
                }
                
        } 
               //modelling Assignment         
           $gbrUpload = request()->file('gambar');
           $gbrName = time().'.'. $gbrUpload->getClientOriginalExtension();
           $gbrPath = public_path('/gambar_artikel/');
           $gbrUpload->move($gbrPath, $gbrName);
            $query = artikelModel::create([
                'judul' => $request->judul,
                'isi' => $request->isi,
                'status' => $request->status,
                'link_seo' => Str::slug ($request->judul),
                'gambar' => $gbrName,
                'id_kategori' => $request->kategori,
                'id_user' => Auth:: id()
            ]);
            $query->tags()->sync($tag_ids);
            $user = Auth::user();
            $user->artikel()->save($query);
        return redirect ('admin/artikel')->with('berhasil','Berhasil diSimpan!');
    }
    public function edit($idArtikel){
        //query builder
       // $query = DB ::table('pertanyaan')->where('id',$idPertanyaan)->first();
       // orm 
       $queryKategori = kategoriModel::all();
       $query = artikelModel :: find($idArtikel);
        //dd($query);
        return view ('adminModul.artikel.edit',compact('query','queryKategori'));
    }
    
    public function update($idArtikel, Request $request ){
      if($request->file('gambar') == "")
        {
           $query = artikelModel :: where('id',$idArtikel)->update([
               "judul" => $request["judul"],
               "isi" => $request["isi"],
               'status' => $request->status,
               'id_kategori' => $request->kategori,
                'link_seo' => Str::slug ($request->judul)
                ]);
           }
           else {
            $data = \App\artikelModel::findOrFail($idArtikel);
            unlink('gambar_artikel/'.$data->gambar);
            $gbrUpload = request()->file('gambar');
            $gbrName = time().'.'. $gbrUpload->getClientOriginalExtension();
            $gbrPath = public_path('/gambar_artikel/');
            $gbrUpload->move($gbrPath, $gbrName);
            $query = artikelModel :: where('id',$idArtikel)->update([
                "judul" => $request["judul"],
                "isi" => $request["isi"],
                'status' => $request->status,
                 'link_seo' => Str::slug ($request->judul),
                 'id_kategori' => $request->kategori,
                 'gambar' => $gbrName
                 ]);
           }
           
              
           //dd($query);
           return redirect ('/admin/artikel')->with('berhasil','Berhasil diUpdate!');
       }
       public function destroy($idArtikel){
        
        //orm
        $data = \App\artikelModel::findOrFail($idArtikel);
        unlink('gambar_artikel/'.$data->gambar);
        ArtikelModel::destroy($idArtikel);    
        return redirect ('/admin/artikel')->with('berhasil','Berhasil diHapus!');

}

      
    }