<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\komentarModel;


class komentarArtikelController extends Controller
{
    public function index (){
        
        $komentar = komentarModel::all();
        return view ('adminModul.komentar_artikel.index', compact('komentar'));
    }

    public function destroy($idkomentar){
        
        //orm
        komentarModel::destroy($idkomentar);    
        return redirect ('/admin/komentarArtikel')->with('success','Berhasil diHapus!');

    }
}
