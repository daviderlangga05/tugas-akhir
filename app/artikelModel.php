<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class artikelModel extends Model
{
    protected $table = "artikel";
 protected $fillable = ["judul", "isi","gambar","status","link_seo","id_kategori","id_user"];
  // protected $guarded = [];

   public function author(){
    return $this->belongsTo('App\User', 'id_user');
    }
    public function kategori(){
        return $this->belongsTo('App\kategoriModel', 'id_kategori');
        }

    public function tags(){
        return $this->belongsToMany('App\tag', 'artikel_tag','artikel_id','tags_id');
    }
 
}
