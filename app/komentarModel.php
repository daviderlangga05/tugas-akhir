<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class komentarModel extends Model
{
    protected $table="komentar_artikel";

    protected $guarded = [];

    public function artikelkomen(){
        return $this->belongsTo('App\artikelModel', 'id_artikel');
        }
}
