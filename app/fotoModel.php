<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class fotoModel extends Model
{
    protected $table="foto";

    protected $guarded = [];
}
