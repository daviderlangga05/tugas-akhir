<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateKomentarArtikelTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('komentar_artikel', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('nama_pengirim')->nullable();
            $table->longText('isi')->nullable();       
            $table->unsignedBigInteger('id_artikel')->nullable();
            $table->foreign('id_artikel')->references('id')->on('artikel');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('komentar_artikel');
    }
}
